from .event import Event2

class FireEvent(Event2):
    NAME = "fire"

    def perform(self):
        if not self.object.has_prop("fireable"):
            self.fail()
            return self.inform("fire.failed")
        self.inform("fire")
