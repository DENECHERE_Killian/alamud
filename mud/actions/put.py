from .action import Action3
from mud.events import PutOnEvent

class PutOnAction(Action3):
    EVENT = PutOnEvent
    RESOLVE_OBJECT = "resolve_for_use"
    RESOLVE_OBJECT2 = "resolve_for_operate"
    ACTION = "put-on"
