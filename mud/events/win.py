from .event import Event1

class WinEvent(Event1):
    NAME = "win"

    def perform(self):
        self.inform("win")
        cont = self.actor.container()
        for x in list(self.actor.contents()):
            x.move_to(cont)
        self.actor.move_to(None)
