# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event3


class BurnWithEvent(Event3):
    NAME = "burn-with"

    def perform(self):
        if not (self.object.has_prop("burnable-with") and self.object2.has_prop("burn")):
            self.fail()
            return self.inform("burn-with.failed")
        self.object.add_prop("burned")
        self.object.move_to(self.actor)
        self.actor.remove(self.object)
        self.inform("burn-with")
