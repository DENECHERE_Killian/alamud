from .action import Action2
from mud.events import ListenEvent

class ListenAction(Action2):
    EVENT = ListenEvent
    ACTION = "listen"
    RESOLVE_OBJECT = "resolve_for_operate"
