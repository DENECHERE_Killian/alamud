# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class EatEvent(Event2):
    NAME = "eat"

    def perform(self):
        if not self.object.has_prop("eatable"):
            self.add_prop("object-not-eatable")
            return self.eat_failed()
        if self.object not in self.actor:
            return self.eat_failed()
        self.actor.remove(self.object)
        self.inform("eat")

    def eat_failed(self):
        self.fail()
        self.inform("eat.failed")
