# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2, Action3
from mud.events import LightOnEvent, LightOffEvent, LightOnWithEvent

class LightOnAction(Action2):
    EVENT = LightOnEvent
    ACTION = "light-on"
    RESOLVE_OBJECT = "resolve_for_operate"

class LightOffAction(Action2):
    EVENT = LightOffEvent
    ACTION = "light-off"
    RESOLVE_OBJECT = "resolve_for_operate"

class LightOnWithAction(Action3):
    EVENT = LightOnWithEvent
    ACTION = "light-on-with"
    RESOLVE_OBJECT = "resolve_for_operate" #si l'item est dans l'environnnement
    RESOLVE_OBJECT2 = "resolve_for_use" #si l'item est dans l'inventaire
