from .event import Event1

class WinAction(Event1):
    NAME = "win-action"

    def perform(self):
        self.inform("win-action")
